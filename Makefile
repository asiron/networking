#CFLAGS=-D_POSIX_SOURCE
OBJ = simulator.o p2.o p3.o p4.o p5.o p6.o simulator_sped_up.o simulator_reord.o
CC=gcc

OBJ_DIR=obj
SRC_DIR=src
BIN_DIR=bin

paper:
	pdflatex paper/main.tex

all:	$(OBJ)
	$(CC) -o $(BIN_DIR)/protocol2 $(OBJ_DIR)/protocol2.o $(OBJ_DIR)/simulator.o
	$(CC) -o $(BIN_DIR)/protocol3 $(OBJ_DIR)/protocol3.o $(OBJ_DIR)/simulator.o
	$(CC) -o $(BIN_DIR)/protocol4 $(OBJ_DIR)/protocol4.o $(OBJ_DIR)/simulator.o
	$(CC) -o $(BIN_DIR)/protocol5 $(OBJ_DIR)/protocol5.o $(OBJ_DIR)/simulator.o
	$(CC) -o $(BIN_DIR)/protocol6 $(OBJ_DIR)/protocol6.o $(OBJ_DIR)/simulator.o
	#Sped up
	$(CC) -o $(BIN_DIR)/protocol2_sped_up $(OBJ_DIR)/protocol2.o $(OBJ_DIR)/simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol3_sped_up $(OBJ_DIR)/protocol3.o $(OBJ_DIR)/simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol4_sped_up $(OBJ_DIR)/protocol4.o $(OBJ_DIR)/simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol5_sped_up $(OBJ_DIR)/protocol5.o $(OBJ_DIR)/simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol6_sped_up $(OBJ_DIR)/protocol6.o $(OBJ_DIR)/simulator_sped_up.o
	#Reordered
	$(CC) -o $(BIN_DIR)/protocol6_reord $(OBJ_DIR)/protocol6.o $(OBJ_DIR)/simulator_reord.o

protocol2:	p2.o simulator.o
	$(CC) -o $(BIN_DIR)/protocol2 $(OBJ_DIR)/protocol2.o $(OBJ_DIR)/simulator.o

protocol3:	p3.o simulator.o
	$(CC) -o $(BIN_DIR)/protocol3 $(OBJ_DIR)/protocol3.o $(OBJ_DIR)/simulator.o

protocol4:	p4.o simulator.o
	$(CC) -o $(BIN_DIR)/protocol4 $(OBJ_DIR)/protocol4.o $(OBJ_DIR)/simulator.o

protocol5:	p5.o simulator.o
	$(CC) -o $(BIN_DIR)/protocol5 $(OBJ_DIR)/protocol5.o $(OBJ_DIR)/simulator.o

protocol6:	p6.o simulator.o
	$(CC) -o $(BIN_DIR)/protocol6 $(OBJ_DIR)/protocol6.o $(OBJ_DIR)/simulator.o

#SPED UP PROTOCOLS
protocol3_sped_up:	p3.o simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol3_sped_up $(OBJ_DIR)/protocol3.o $(OBJ_DIR)/simulator_sped_up.o

protocol4_sped_up:	p4.o simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol4_sped_up $(OBJ_DIR)/protocol4.o $(OBJ_DIR)/simulator_sped_up.o

protocol5_sped_up:	p5.o simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol5_sped_up $(OBJ_DIR)/protocol5.o $(OBJ_DIR)/simulator_sped_up.o

protocol6_sped_up:	p6.o simulator_sped_up.o
	$(CC) -o $(BIN_DIR)/protocol6_sped_up $(OBJ_DIR)/protocol6.o $(OBJ_DIR)/simulator_sped_up.o

clean:	
	rm -f obj/*.o *.bak
	rm -f bin/protocol*

simulator_sped_up.o:	$(SRC_DIR)/simulator_sped_up.c
	$(CC) -c -o $(OBJ_DIR)/simulator_sped_up.o $(SRC_DIR)/simulator_sped_up.c

simulator_reord.o:	$(SRC_DIR)/simulator_reord.c
	$(CC) -c -o $(OBJ_DIR)/simulator_reord.o $(SRC_DIR)/simulator_reord.c 

simulator.o:	$(SRC_DIR)/simulator.c
	$(CC) -c -o $(OBJ_DIR)/simulator.o $(SRC_DIR)/simulator.c 

p2.o:	$(SRC_DIR)/p2.c
	$(CC) -c -o $(OBJ_DIR)/protocol2.o $(SRC_DIR)/p2.c
p3.o: $(SRC_DIR)/p3.c
	$(CC) -c -o $(OBJ_DIR)/protocol3.o $(SRC_DIR)/p3.c
p4.o: $(SRC_DIR)/p4.c
	$(CC) -c -o $(OBJ_DIR)/protocol4.o $(SRC_DIR)/p4.c
p5.o: $(SRC_DIR)/p5.c
	$(CC) -c -o $(OBJ_DIR)/protocol5.o $(SRC_DIR)/p5.c
p6.o: $(SRC_DIR)/p6.c
	$(CC) -c -o $(OBJ_DIR)/protocol6.o $(SRC_DIR)/p6.c
