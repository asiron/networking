#!/bin/zsh

#Requires Z-Shell to run
SIM_TIME=20000


#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_checksum_err_payloads_reord;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo 0 5 15 30` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --aggregate protocol6 protocol6_reord  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale

exit 1

echo "#EXC2 5 6 retransmitted=(timeout, checksum_err)"
#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_checksum_err_retransmitted_2;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo {0..21..3}` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME --logscale

echo "#EXC2 5 6 retransmitted=(timeout, lost_packet)"
#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_lost_packet_retransmitted_2;
./run_sim_tests.py $DIR --xs timeout --ys lost_packet --checksum-err 0 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet `echo {0..21..3}` --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME --logscale


echo "#EXC2 5 6 retransmitted=(timeout, delay)"
#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_delay_retransmitted;
./run_sim_tests.py $DIR --xs timeout --ys delay --checksum-err 10 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 10 --delay `echo 0 10 20 40 100` --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME --logscale

echo "#EXC2 5 6 retransmitted=(timeout, delay)"
#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_delay_payloads;
./run_sim_tests.py $DIR --xs timeout --ys delay --checksum-err 10 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 10 --delay `echo 0 10 20 40 100` --ps protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale


exit 1


#Exercise 2
#AGG
echo "#EXC2 5 6 efficiency=(checksum_err, timeout)"
#EXC2 5 6 efficiency=(checksum_err, timeout)
DIR=exc2_5_6_checksum_err_timeout_efficiency_agg;
./run_sim_tests.py $DIR --xs checksum_err --ys timeout --checksum-err `echo {0..99}` --timeout `echo 20 40 80 150` --lost-packet 0 --delay 0 --aggregate protocol5 protocol6  --plot-type 2d --result-field efficiency --sim-time $SIM_TIME

echo "#EXC2 5 6 payloads=(lost_packet, timeout)"
#EXC2 5 6 payloads=(lost_packet, timeout)
DIR=exc2_5_6_lost_packet_timeout_payloads_agg;
./run_sim_tests.py $DIR --xs lost_packet --ys timeout --lost-packet `echo {0..99}` --timeout `echo 20 40 80 150` --checksum-err 0 --delay 0 --aggregate protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME

echo "#EXC2 5 6 retransmitted=(checksum_err, timeout)"
#EXC2 5 6 retransmitted=(checksum_err, timeout)
DIR=exc2_5_6_checksum_err_timeout_retransmitted_agg;
./run_sim_tests.py $DIR --xs checksum_err --ys timeout --checksum-err `echo {0..99}` --timeout `echo 20 40 80 150` --lost-packet 0 --delay 0 --aggregate protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME

exit 1

echo "LOST PACK"
echo "#EXC2 5 6 efficiency=(lost_packet, timeout)"
#EXC2 5 6 efficiency=(lost_packet, timeout)
DIR=exc2_5_6_lost_packet_timeout_efficiency;
./run_sim_tests.py $DIR --xs lost_packet --ys timeout --lost-packet `echo {0..99}` --timeout `echo {5..15..5} 25 50` --checksum-err 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field efficiency --sim-time $SIM_TIME

echo "#EXC2 5 6 payloads=(lost_packet, timeout)"
#EXC2 5 6 payloads=(lost_packet, timeout)
DIR=exc2_5_6_lost_packet_timeout_payloads;
./run_sim_tests.py $DIR --xs lost_packet --ys timeout --lost-packet `echo {0..99}` --timeout `echo {5..15..5} 25 50` --checksum-err 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME

echo "#EXC2 5 6 retransmitted=(lost_packet, timeout)"
#EXC2 5 6 retransmitted=(lost_packet, timeout)
DIR=exc2_5_6_lost_packet_timeout_retransmitted;
./run_sim_tests.py $DIR --xs lost_packet --ys timeout --lost-packet `echo {0..99}` --timeout `echo {5..15..5} 25 50` --checksum-err 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME

exit 1

echo "#EXC2 5 6 efficiency=(checksum_err, timeout)"
#EXC2 5 6 efficiency=(checksum_err, timeout)
DIR=exc2_5_6_checksum_err_timeout_efficiency;
./run_sim_tests.py $DIR --xs checksum_err --ys timeout --checksum-err `echo {0..99}` --timeout `echo {5..15..5} 25 50` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field efficiency --sim-time $SIM_TIME

echo "#EXC2 5 6 payloads=(checksum_err, timeout)"
#EXC2 5 6 payloads=(checksum_err, timeout)
DIR=exc2_5_6_checksum_err_timeout_payloads;
./run_sim_tests.py $DIR --xs checksum_err --ys timeout --checksum-err `echo {0..99}` --timeout `echo {5..15..5} 25 50` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME

echo "#EXC2 5 6 retransmitted=(checksum_err, timeout)"
#EXC2 5 6 retransmitted=(checksum_err, timeout)
DIR=exc2_5_6_checksum_err_timeout_retransmitted;
./run_sim_tests.py $DIR --xs checksum_err --ys timeout --checksum-err `echo {0..99}` --timeout `echo {5..15..5} 25 50` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME

exit 1

echo "#EXC2 5 6 retransmitted=(timeout, checksum_err)"
#EXC2 5 6 retransmitted=(timeout, checksum_err)
DIR=exc2_5_6_timeout_checksum_err_retransmitted;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo {0..21..3}` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field retransmitted --sim-time $SIM_TIME --logscale --only-one-proc

echo "#EXC2 5 6 efficiency=(timeout, lost_packet)"
#EXC2 5 6 efficiency=(timeout, lost_packet)
DIR=exc2_5_6_timeout_lost_efficiency;
./run_sim_tests.py $DIR --xs timeout --ys lost_packet --checksum-err 0 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet `echo {0..21..3}` --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field efficiency --sim-time $SIM_TIME --logscale --only-one-proc

echo "#EXC2 5 6 efficiency=(timeout, checksum_err)"
#EXC2 5 6 efficiency=(timeout, checksum_err)
DIR=exc2_5_6_timeout_checksum_err_efficiency;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo {0..21..3}` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field efficiency --sim-time $SIM_TIME --logscale --only-one-proc

echo "#EXC2 5 6 payloads=(timeout, lost_packet)"
#EXC2 5 6 payloads=(timeout, lost_packet)
DIR=exc2_5_6_timeout_lost_payloads;
./run_sim_tests.py $DIR --xs timeout --ys lost_packet --checksum-err 0 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet `echo {0..21..3}` --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale --only-one-proc

echo "#EXC2 5 6 payloads=(timeout, checksum_err)"
#EXC2 5 6 payloads=(timeout, checksum_err)
DIR=exc2_5_6_timeout_checksum_payloads;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo {0..21..3}` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --ps protocol5 protocol6  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale --only-one-proc

echo "#EXC2 5 6 payloads=(checksum, lost_packet) 3D"
#EXC2 5 6 payloads=(checksum, lost_packet) 3D
DIR=exc2_5_6_3d_lost_checksum_err_payloads;
./run_sim_tests.py $DIR --xs lost_packet --ys checksum_err --checksum-err `echo {0..30}` --timeout 50 --lost-packet `echo {0..30}` --delay 0 --ps protocol5 protocol6  --plot-type 3d --result-field payloads --sim-time 50000 --only-one-proc

echo "#EXC2 5 6 efficiency=(timeout, lost_packet) 3D"
#EXC2 5 6 efficiency=(timeout, lost_packet) 3D
DIR=exc2_5_6_3d_lost_checksum_err_efficiency;
./run_sim_tests.py $DIR --xs lost_packet --ys checksum_err --checksum-err `echo {0..30}` --timeout 50 --lost-packet `echo {0..30}` --delay 0 --ps protocol5 protocol6  --plot-type 3d --result-field efficiency --sim-time 50000 --only-one-proc

echo "#EXC2 5 6 retransmitted=(timeout, lost_packet) 3D"
#EXC2 5 6 retransmitted=(timeout, lost_packet) 3D
DIR=exc2_5_6_3d_lost_checksum_err_retransmitted;
./run_sim_tests.py $DIR --xs lost_packet --ys checksum_err --checksum-err `echo {0..30}` --timeout 50 --lost-packet `echo {0..30}` --delay 0 --ps protocol5 protocol6  --plot-type 3d --result-field retransmitted --sim-time 50000 --only-one-proc

#Exercise 1

#3D graph 3 and 4 EXC 1
DIR=exc1_3_4_3d_lost_checksum_err_3;
./run_sim_tests.py $DIR --xs lost_packet --ys checksum_err --checksum-err `echo {0..30}` --timeout 50 --lost-packet `echo {0..30}` --delay 0 --ps protocol3 protocol4  --plot-type 3d --result-field payloads --sim-time 50000 --only-one-proc

#Aggregate 3 and 4 EXC 1
DIR=exc1_3_4_agg_timeout_checksum_err;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo 0 5 10 20` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --aggregate protocol3 protocol4  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale --only-one-proc
#exit 1

#Payloads(x=timeout, y=lost_packet)
DIR=exc1_3_4_timeout_lost_payloads;
./run_sim_tests.py $DIR --xs timeout --ys lost_packet --checksum-err 0 --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet `echo {0..21..3}` --delay 0 --ps protocol3 protocol4  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale --only-one-proc

#Payloads(x=timeout, y=checksum_err)
DIR=exc1_3_4_timeout_checksum_err;
./run_sim_tests.py $DIR --xs timeout --ys checksum_err --checksum-err `echo {0..21..3}` --timeout `echo {1..49} {50..90..10} {100..1000..100}` --lost-packet 0 --delay 0 --ps protocol3 protocol4  --plot-type 2d --result-field payloads --sim-time $SIM_TIME --logscale --only-one-proc

