set terminal epslatex
set output 'exc1_3_4_agg_timeout_checksum_err/protocol3_protocol4_agg/exc1_3_4_agg_timeout_checksum_err.tex'
set xlabel 'Timeout [s]'
input_file='exc1_3_4_agg_timeout_checksum_err/protocol3_protocol4_agg/result.dat'
set ylabel 'Accepted Payloads '
set logscale x 10
set key spacing 3.0
set key font ",8"
plot input_file u 1:2 w l title '\scriptsize{P3 Chk-sum Error 0 \%}', input_file u 1:3 w l title '\scriptsize{P4 Chk-sum Error 0 \%}', input_file u 1:4 w l title '\scriptsize{P3 Chk-sum Error 5 \%}', input_file u 1:5 w l title '\scriptsize{P4 Chk-sum Error 5 \%}', input_file u 1:6 w l title '\scriptsize{P3 Chk-sum Error 10 \%}', input_file u 1:7 w l title '\scriptsize{P4 Chk-sum Error 10 \%}', input_file u 1:8 w l title '\scriptsize{P3 Chk-sum Error 20 \%}', input_file u 1:9 w l title '\scriptsize{P4 Chk-sum Error 20 \%}'
