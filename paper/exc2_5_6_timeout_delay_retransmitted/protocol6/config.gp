set terminal epslatex
set output 'exc2_5_6_timeout_delay_retransmitted/protocol6/exc2_5_6_timeout_delay_retransmitted.tex'
set xlabel 'Timeout [s]'
input_file='exc2_5_6_timeout_delay_retransmitted/protocol6/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Retransmitted Frames '
set logscale x 10
plot input_file u 1:2 w l title '\scriptsize{Propagation Delay 0 [s]}', input_file u 1:3 w l title '\scriptsize{Propagation Delay 10 [s]}', input_file u 1:4 w l title '\scriptsize{Propagation Delay 20 [s]}', input_file u 1:5 w l title '\scriptsize{Propagation Delay 40 [s]}', input_file u 1:6 w l title '\scriptsize{Propagation Delay 100 [s]}'
