set terminal epslatex size 5.0in,6.0in font ',8'
set output 'exc1_3_4_3d_lost_checksum_err_3/protocol4/exc1_3_4_3d_lost_checksum_err_3.tex'
set xlabel 'Loss Rate \%'
input_file='exc1_3_4_3d_lost_checksum_err_3/protocol4/result.dat'
set ticslevel 0
set key spacing 3.0
set key font ",8"
set grid nopolar
set grid xtics nomxtics ytics nomytics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   lt black linewidth 0.200 dashtype solid,  lt black linewidth 0.200 dashtype solid
set ylabel 'Chk-sum Error \%'
set zlabel 'Accepted Payloads ' offset 0,0
set view 60,40
splot input_file u 1:2:3 title "" with points #pm3d palette
