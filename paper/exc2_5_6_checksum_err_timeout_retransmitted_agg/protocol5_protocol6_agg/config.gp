set terminal epslatex
set output 'exc2_5_6_checksum_err_timeout_retransmitted_agg/protocol5_protocol6_agg/exc2_5_6_checksum_err_timeout_retransmitted_agg.tex'
set xlabel 'Chk-sum Error \%'
input_file='exc2_5_6_checksum_err_timeout_retransmitted_agg/protocol5_protocol6_agg/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Retransmitted Frames '
plot input_file u 1:2 w l title '\scriptsize{P5 Timeout 20 [s]}', input_file u 1:3 w l title '\scriptsize{P6 Timeout 20 [s]}', input_file u 1:4 w l title '\scriptsize{P5 Timeout 40 [s]}', input_file u 1:5 w l title '\scriptsize{P6 Timeout 40 [s]}', input_file u 1:6 w l title '\scriptsize{P5 Timeout 80 [s]}', input_file u 1:7 w l title '\scriptsize{P6 Timeout 80 [s]}', input_file u 1:8 w l title '\scriptsize{P5 Timeout 150 [s]}', input_file u 1:9 w l title '\scriptsize{P6 Timeout 150 [s]}'
