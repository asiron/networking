set terminal epslatex
set output 'exc2_5_6_timeout_checksum_err_payloads_reord/protocol6_protocol6_reord_agg/exc2_5_6_timeout_checksum_err_payloads_reord.tex'
set xlabel 'Timeout [s]'
input_file='exc2_5_6_timeout_checksum_err_payloads_reord/protocol6_protocol6_reord_agg/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Accepted Payloads '
set logscale x 10
plot input_file u 1:2 w l title '\scriptsize{P6 Chk-sum Error 0 \%}', input_file u 1:3 w l title '\scriptsize{P6 reord Chk-sum Error 0 \%}', input_file u 1:4 w l title '\scriptsize{P6 Chk-sum Error 5 \%}', input_file u 1:5 w l title '\scriptsize{P6 reord Chk-sum Error 5 \%}', input_file u 1:6 w l title '\scriptsize{P6 Chk-sum Error 15 \%}', input_file u 1:7 w l title '\scriptsize{P6 reord Chk-sum Error 15 \%}', input_file u 1:8 w l title '\scriptsize{P6 Chk-sum Error 30 \%}', input_file u 1:9 w l title '\scriptsize{P6 reord Chk-sum Error 30 \%}'
