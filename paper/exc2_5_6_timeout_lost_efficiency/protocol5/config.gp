set terminal epslatex
set output 'exc2_5_6_timeout_lost_efficiency/protocol5/exc2_5_6_timeout_lost_efficiency.tex'
set xlabel 'Timeout [s]'
input_file='exc2_5_6_timeout_lost_efficiency/protocol5/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Efficiency \%'
set logscale x 10
plot input_file u 1:2 w l title '\scriptsize{Loss Rate 0 \%}', input_file u 1:3 w l title '\scriptsize{Loss Rate 3 \%}', input_file u 1:4 w l title '\scriptsize{Loss Rate 6 \%}', input_file u 1:5 w l title '\scriptsize{Loss Rate 9 \%}', input_file u 1:6 w l title '\scriptsize{Loss Rate 12 \%}', input_file u 1:7 w l title '\scriptsize{Loss Rate 15 \%}', input_file u 1:8 w l title '\scriptsize{Loss Rate 18 \%}', input_file u 1:9 w l title '\scriptsize{Loss Rate 21 \%}'
