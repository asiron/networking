\contentsline {section}{Introduction}{3}{section*.2}
\contentsline {subsection}{Graph Generation Tool}{3}{section*.3}
\contentsline {section}{Exercise 1}{3}{section*.4}
\contentsline {subsection}{Protocol 3 - Stop--and--Wait for Noisy Channel}{3}{section*.5}
\contentsline {subsubsection}{P3: Timeout and Loss Package Rate}{4}{section*.6}
\contentsline {subsubsection}{P3: Timeout and Checksum Error Rate}{5}{section*.7}
\contentsline {subsubsection}{P3: Both Error Rates}{5}{section*.8}
\contentsline {subsection}{Protocol 4 - One--bit Sliding Window}{5}{section*.9}
\contentsline {subsubsection}{P4: Timeout and Loss Packet Rate}{6}{section*.10}
\contentsline {subsubsection}{P4: Timeout and Checksum Error Rate}{7}{section*.11}
\contentsline {subsubsection}{P4: Both Error Rates}{7}{section*.12}
\contentsline {subsection}{Protocol 3 and 4 - Comparison}{8}{section*.13}
\contentsline {section}{Exercise 2}{8}{section*.14}
\contentsline {subsection}{Protocol 5 - Go--Back--N}{8}{section*.15}
\contentsline {subsubsection}{P5: Efficiency with Checksum Error Rate and Timeout}{9}{section*.16}
\contentsline {subsubsection}{P5: Payloads with Loss Packet Rate and Timeout}{11}{section*.17}
\contentsline {subsubsection}{P5: Retransmitted Frames with Checksum Error Rate and Timeout}{11}{section*.18}
\contentsline {subsection}{Protocol 5 - Selective Repeat}{11}{section*.19}
\contentsline {subsubsection}{P6: Efficiency with Checksum Error Rate and Timeout}{12}{section*.20}
\contentsline {subsubsection}{P6: Payloads with Loss Packet Rate and Timeout}{12}{section*.21}
\contentsline {subsubsection}{P6: Retransmitted Frames with Checksum Error Rate and Timeout}{12}{section*.22}
\contentsline {subsection}{Protocol 5 and 6 - Comparison}{14}{section*.23}
\contentsline {subsubsection}{P6: Efficiency with Checksum Error Rate and Timeout}{14}{section*.24}
\contentsline {subsubsection}{P6: Payloads with Loss Packet Rate and Timeout}{14}{section*.25}
\contentsline {subsubsection}{P6: Retransmitted Frames with Checksum Error Rate and Timeout}{14}{section*.26}
\contentsline {section}{Exercise 3}{16}{section*.27}
\contentsline {section}{Exercise 4}{17}{section*.28}
\contentsline {subsection}{Protocol 5 - Optimum for retransmitted frames}{17}{section*.29}
\contentsline {subsubsection}{Protocol 5 - Timeout and Checksum Error Rate}{17}{section*.30}
\contentsline {subsubsection}{Protocol 6 - Timeout and Checksum Error Rate}{17}{section*.31}
\contentsline {subsubsection}{Protocol 6 - Timeout and Loss Packet Rate}{17}{section*.32}
\contentsline {section}{Exercise 5}{19}{section*.33}
\contentsline {section}{Exercise 6}{20}{section*.34}
\contentsline {subsection}{Protocol 5 - Accepted Payloads with respect to Timeout and Delay with Constant Error Rates}{20}{section*.35}
\contentsline {subsection}{Protocol 6 - Accepted Payloads with respect to Timeout and Delay with Constant Error Rates}{20}{section*.36}
\contentsline {section}{\\Appendix A - Simulator Modifications}{21}{section*.37}
\contentsline {subsection}{Exercise 5. Simulation Speed Up}{21}{section*.38}
\contentsline {subsection}{Exercise 6. Adding propagation delay}{22}{section*.39}
\contentsline {section}{\\Appendix B - Graph Generation Tool}{23}{section*.40}
