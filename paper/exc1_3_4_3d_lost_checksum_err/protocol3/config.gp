set terminal epslatex size 5.0in,6.0in font ',8'
set output 'exc1_3_4_3d_lost_checksum_err/protocol3/exc1_3_4_3d_lost_checksum_err.tex'
set xlabel 'Loss Rate \%'
input_file='exc1_3_4_3d_lost_checksum_err/protocol3/result.dat'
set ticslevel 0
set key spacing 3.0
set key font ",8"
set parametric
set ylabel 'Chk-sum Error \%'
set zlabel 'Accepted Payloads '
set dgrid3d
set pm3d corners2color mean interpolate 4,4
set view 60,15
set palette color
splot input_file u 1:2:3 title "" with pm3d palette
