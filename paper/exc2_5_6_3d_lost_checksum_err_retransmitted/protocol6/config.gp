set terminal epslatex
set output 'exc2_5_6_3d_lost_checksum_err_retransmitted/protocol6/exc2_5_6_3d_lost_checksum_err_retransmitted.tex'
set xlabel 'Loss Rate \%'
input_file='exc2_5_6_3d_lost_checksum_err_retransmitted/protocol6/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Chk-sum Error \%'
set zlabel 'Retransmitted Frames '
splot input_file u 1:2:3 title ""
