set terminal epslatex
set output 'exc2_5_6_lost_packet_timeout_efficiency/protocol6/exc2_5_6_lost_packet_timeout_efficiency.tex'
set xlabel 'Loss Rate \%'
input_file='exc2_5_6_lost_packet_timeout_efficiency/protocol6/result.dat'
set key spacing 3.0
set key font ",8"
set ylabel 'Efficiency \%'
plot input_file u 1:2 w l title '\scriptsize{Timeout 5 [s]}', input_file u 1:3 w l title '\scriptsize{Timeout 10 [s]}', input_file u 1:4 w l title '\scriptsize{Timeout 15 [s]}', input_file u 1:5 w l title '\scriptsize{Timeout 25 [s]}', input_file u 1:6 w l title '\scriptsize{Timeout 50 [s]}'
